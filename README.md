# Frontend Mentor - Calculator app

![Design preview for the Calculator app coding challenge](./design/desktop-preview.jpg)

## Links

- Live site: <https://fem-calculator-five.vercel.app/>
- Solution page: <https://www.frontendmentor.io/solutions/vintage-calculator-of-your-dreams-with-react-typescript-sass-BJJ11AgH5>

## Notes

Kinda proud of this one. In the previous challenge I encountered strange(for me) behaviour of `type=number` inputs with react+ts, and this time I decided to do all validations manually. It ended up pretty wild, I basically hardcoded every button's behaviour in every situation following my idea of how a real calculator should behave. I like how it came together, this is not the code to be proud of, but it does the job well! I could find only one sequence of actions that lead to `NaN` on the calculator screen, and it's not a straightforward one(can you find it?)

The stuff I wanted, but failed to implement:

- Make screen overflow to the left and stay aligned to the right when there are lengthy numbers. It's working now with `justify-content: flex-end` but for some reason it's not scrollable with flex. Without flex, `overflow: auto` does it's job, but it overflows to the right despite that the content is aligned to the right. I tried approaches with `direction: rtl` - no luck, I tried `scrollIntoView` - it kinda does the job, but it also scrolls vertically and that's bad UX in my opinion. I went with flex in the end.
- Issue related to the previous one - what to do with number with a lot of digits? Given that I chose flex with no scroll I decided to round decimals to 12 digits so a number like 0.333(3) is visible on the screen. It doesn't work for big numbers but it's something. I also made the screen expandable into two lines when there are long numbers, not sure about this decision.
- How to handle all ⌨keyboard 👇events of the whole page? Now keyhandler is on the main div, but if we click away it loses focus. We can't put a keyhandler on the body, can we?

## Stuff I learned during this project

- Getting values from `localStorage` and setting them as an initial value for `useState` hook. Not sure the way I did is the best practice, but it worked well.
- Implementing theme switch via imperative class toggling of `body` in `useEffect` hook.
- Used `useRef` hook to grab focus after first render:
  ```js
  const appRef = useRef<HTMLDivElement>(null)

  useEffect(() => appRef.current?.focus(), [])
  ```
- Kinda answering my own question about where to put key handler callback. Dan Abramov in this [video](https://youtu.be/Qxn4-bTOx0g?t=4232) puts a handler on a `window` object in `useEffect` ([source code](https://gist.github.com/gaearon/b543da43290b15c514493da86958d913#file-wordle-js)), so I guess if Dan is doing it, it's not the worst practice.
- Triggering a oneshot animation via `startAnim` prop and `setStartAnim` callback prop which is placed on `onAnimationEnd`. Not sure whether it's a react way to trigger animation, but that's how I would do it in Godot game engine 🤖
