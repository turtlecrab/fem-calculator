import { MouseEventHandler } from 'react'
import '../Calc.scss'

interface Props {
  theme: number,
  handleThemeSwitch: MouseEventHandler
}

function ThemeSwitcher({ theme, handleThemeSwitch }: Props) {
  return (
    <div className="theme">
      <span className="themeLabel">THEME</span>
      <button
        className='themeButton'
        aria-label='Select next visual theme'
        onClick={handleThemeSwitch}
      >
        <div className='themeNums' aria-hidden="true">
          <span>1</span>
          <span>2</span>
          <span>3</span>
        </div>
        <div className={'themeIcon themeIcon-' + theme}></div>
      </button>
    </div>
  )
}

export default ThemeSwitcher
