import './animations.scss'
import '../Calc.scss'

const symbols = {
  '+': '+',
  '-': '−',
  '/': '÷',
  'x': '×',
}

interface Props {
  left: string,
  right: string,
  operator: string,
  startZeroAnim: boolean,
  setStartZeroAnim: React.Dispatch<React.SetStateAction<boolean>>
}

function Screen({ left, right, operator, startZeroAnim, setStartZeroAnim }: Props) {
  const zeroDivAnim = startZeroAnim ? 'animate__animated animate__flash' : ''
  return (
    <h2 className="screen">
      <span
        className={zeroDivAnim}
        onAnimationEnd={() => setStartZeroAnim(false)}
      >
        {left.replace(/\./g, ',').replace(/-/g, '−')}
        {operator && (
          <span className="screenOperator">
            {symbols[operator as '+' | '-' | '/' | 'x']}
          </span>
        )}
        {right.replace(/\./g, ',').replace(/-/g, '−')}
      </span>
    </h2>
  )
}

export default Screen
