import { MouseEventHandler } from 'react'
import '../Calc.scss'

interface Props {
  action: string,
  handleKey: MouseEventHandler
}

function Key({ action, handleKey }: Props) {
  const classNames = ['key']

  if (action === 'DEL') {
    classNames.push('key--reset')
  } else if (action === 'RESET') {
    classNames.push('key--reset')
    classNames.push('key--wide')
  } else if (action === '=') {
    classNames.push('key--equals')
    classNames.push('key--wide')
  }

  return (
    <button
      className={classNames.join(' ')}
      value={action}
      onClick={handleKey}
    >
      {action}
    </button>
  )
}

export default Key
