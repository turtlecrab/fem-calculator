import { KeyboardEvent, MouseEvent, useEffect, useRef, useState } from 'react'
import './Calc.scss'

import ThemeSwitcher from './components/ThemeSwitcher'
import Screen from './components/Screen'
import Key from './components/Key'

function reduceOperation(left: string, right: string, operator: string): string {
  if (right === '-') right = ''
  if (left === '-') left = ''

  if (!right) return left
  if (operator === '+') return toFixed(parseFloat(left) + parseFloat(right))
  if (operator === '-') return toFixed(parseFloat(left) - parseFloat(right))
  if (operator === 'x') return toFixed(parseFloat(left) * parseFloat(right))
  if (operator === '/') {
    if (parseFloat(right) === 0) {
      return left
    }
    return toFixed(parseFloat(left) / parseFloat(right))
  }
  return ''
}

function toFixed(num: number): string {
  const fixed = 12
  return String(parseFloat(num.toFixed(fixed)))
}

function Calc() {
  const [theme, setTheme] = useState<number>(() => {
    const saved = localStorage.getItem('theme')
    if (saved && /^\d+$/.test(saved)) {
      return Number(saved) % 3
    }
    const isDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches
    return isDarkMode ? 2 : 0
  })

  const [left, setLeft] = useState<string>('')
  const [right, setRight] = useState<string>('')
  const [operator, setOperator] = useState<string>('')

  const [justCalculated, setJustCalculated] = useState(false)
  const [startZeroAnim, setStartZeroAnim] = useState(false)

  const appRef = useRef<HTMLDivElement>(null)

  useEffect(() => appRef.current?.focus(), [])
  
  useEffect(() => {
    localStorage.setItem('theme', String(theme))
    document.body.className = ''
    document.body.classList.add('theme-' + theme)
  }, [theme])

  function handleThemeSwitch() {
    setTheme((theme + 1) % 3)
  }
  
  function handleKeyboard(e: KeyboardEvent<HTMLDivElement>) {
    let key = e.key
    if (
      /^[\d.,/+\-x*=]$/.test(key) ||
      key === 'Backspace' ||
      key === 'Delete' ||
      key === 'Escape' ||
      key === 'Enter' 
    ) {
      if (key === 'Backspace' || key === 'Delete') key = 'DEL'
      else if (key === 'Escape') key = 'RESET'
      else if (key === ',') key = '.'
      else if (key === '*') key = 'x'
      else if (key === 'Enter') {
        e.preventDefault()
        key = '='
      }
      handleKey(key)
    }
  }

  function handleKeyUI(e: MouseEvent<HTMLButtonElement>) {
    handleKey(e.currentTarget.value)
  }

  function handleKey(key: string) {
    if (key === 'RESET') {
      setLeft('')
      setRight('')
      setOperator('')
    }
    else if (key === 'DEL') {
      if (right) setRight(right.slice(0, -1))
      else if (operator) setOperator('')
      else setLeft(left.slice(0, -1))
    }
    else if (key === '=') {
      if (parseFloat(right) === 0 && operator === '/') {
        setStartZeroAnim(true)
        return
      }
      if (!operator) return // so justCalculated is not set
      setLeft(reduceOperation(left, right, operator))
      setRight('')
      setOperator('')
    }
    else if (/[1-9]/.test(key)) {
      if (operator) {
        if (right === '0') setRight(key)
        else if (right === '-0') setRight('-' + key)
        else setRight(right + key)
      }
      else {
        if (justCalculated) setLeft(key)
        else {
          if (left === '0') setLeft(key)
          else if (left === '-0') setLeft('-' + key)
          else setLeft(left + key)
        }
      }
    }
    else if (key === '0') {
      if (operator) {
        if (right !== '0' && right !== '-0') setRight(right + '0')
      }
      else {
        if (justCalculated) setLeft('0')
        else {
          if (left !== '0' && left !== '-0') setLeft(left + '0')
        }
      }
    }
    else if (key === '.') {
      if (operator) {
        if (!right.includes('.')) {
          if (right === '-') setRight('-0.')
          else setRight((right || '0') + '.')
        }
      }
      else {
        if (justCalculated) setLeft('0.')
        else if (!left.includes('.')) {
          if (left === '-') setLeft('-0.')
          else setLeft((left || '0') + '.')
        }
      }
    }
    else if (/[/+x]/.test(key)) {
      if (left && left !== '-') {
        if (right && right !== '-') {
          setLeft(reduceOperation(left, right, operator))
          setRight('')
        }
        else setLeft(String(parseFloat(left)))
        setOperator(key)
        setRight('')
      }
      else if (left === '-') setLeft('')
    }
    else if (key === '-') {
      if (right) {
        setLeft(reduceOperation(left, right, operator))
        setRight('')
        setOperator('-')
      }
      else if (operator) setRight('-')
      else if (left === '-') setLeft('')
      else if (left) {
        setLeft(String(parseFloat(left)))
        setOperator('-')
      }
      else setLeft('-')
    }
    setJustCalculated(key === '=')
  }

  return (
    <div className="calc" onKeyDown={handleKeyboard} tabIndex={0} ref={appRef}>
      <div className="headerRow">
        <h1 className="heading">calc</h1>
        <ThemeSwitcher
          theme={theme}
          handleThemeSwitch={handleThemeSwitch}
        />
      </div>
      <Screen
        left={left}
        right={right}
        operator={operator}
        startZeroAnim={startZeroAnim}
        setStartZeroAnim={setStartZeroAnim}
      />
      <div className="keys">
        <Key action="7" handleKey={handleKeyUI} />
        <Key action="8" handleKey={handleKeyUI} />
        <Key action="9" handleKey={handleKeyUI} />
        <Key action="DEL" handleKey={handleKeyUI} />
        <Key action="4" handleKey={handleKeyUI} />
        <Key action="5" handleKey={handleKeyUI} />
        <Key action="6" handleKey={handleKeyUI} />
        <Key action="+" handleKey={handleKeyUI} />
        <Key action="1" handleKey={handleKeyUI} />
        <Key action="2" handleKey={handleKeyUI} />
        <Key action="3" handleKey={handleKeyUI} />
        <Key action="-" handleKey={handleKeyUI} />
        <Key action="." handleKey={handleKeyUI} />
        <Key action="0" handleKey={handleKeyUI} />
        <Key action="/" handleKey={handleKeyUI} />
        <Key action="x" handleKey={handleKeyUI} />
        <Key action="RESET" handleKey={handleKeyUI} />
        <Key action="=" handleKey={handleKeyUI} />

      </div>
    </div>
  )
}

export default Calc
